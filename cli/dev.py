import subprocess
import shlex
from time import sleep
from collections import namedtuple
from pathlib import Path
import click


SubProcess = namedtuple("SubProcess", ["is_active", "cmd"])
DockerService = namedtuple("DockerCompose", ["name", "is_active", "path"])


@click.command()
@click.option(
    "--db/--no-db", default=True, show_default=True, help="Start db containers.",
)
@click.option(
    "--worker/--no-worker", default=True, show_default=True, help="Start workers.",
)
@click.option(
    "--worker-log-level",
    default="WARNING",
    show_default=True,
    help="Logger level for workers.",
)
@click.option(
    "--ms-plot/--no-ms-plot", default=True, show_default=True, help="Start workers.",
)
def run(db, worker, worker_log_level, ms_plot):

    backend_path = Path(__file__).parents[1].resolve()
    container_path = backend_path.parents[1].resolve() / "Containers"

    version = (backend_path / "VERSION").read_text()
    print("version", repr(version))

    subprocesses_data = (
        SubProcess(
            worker,
            (
                "conda run --no-capture-output --live-stream -n metwork "
                f"celery worker -A metwork_backend -Q web.{version},run.{version} "
                f"--concurrency=2 -l {worker_log_level}"
            ),
        ),
    )

    services_data = (
        DockerService(
            "hoster", True, f"{backend_path}/docker/hoster.docker-compose.yml",
        ),
        DockerService("db", db, f"{backend_path}/docker/db.docker-compose.yml",),
        DockerService(
            "broker", worker, f"{backend_path}/docker/broker.docker-compose.yml",
        ),
        DockerService(
            "ms_plot", ms_plot, f"{container_path}/ms-plot/docker-compose.yml",
        ),
    )

    try:

        containers = []
        if db:
            containers.append("database")
        if worker:
            containers += ["broker", "cache"]
        if containers:
            containers.append("docker_hoster")

        main_cmd = (
            "conda run --no-capture-output --live-stream -n metwork "
            f"{backend_path}/manage.py runserver 0.0.0.0:8000"
        )

        services = []
        for service_data in services_data:
            if service_data.is_active:
                services.append(ServiceManager(service_data))

        subprocesses = []
        for sp_data in subprocesses_data:
            if sp_data.is_active:
                subprocesses.append(SubProcessManager(sp_data.cmd))
        main_process = SubProcessManager(main_cmd)
        while True:
            pass
    except KeyboardInterrupt:
        for subprocess_ in subprocesses:
            subprocess_.close()
        main_process.wait_for_close()

        for service in services:
            service.close()

    print("")


class ServiceManager:
    def __init__(self, data: DockerService):
        self.name = data.name
        self.path = data.path
        cmd = f"docker-compose -p {self.name} -f {self.path} up -d"
        self.run(cmd)

    def run(self, cmd):
        subprocess.run(shlex.split(cmd))

    def close(self):
        cmd = f"docker-compose -p {self.name} -f {self.path} stop"
        self.run(cmd)


class SubProcessManager:
    def __init__(self, cmd: str):
        self.cmd = cmd
        self.process = subprocess.Popen(
            shlex.split(cmd),
            stdout=subprocess.PIPE,
            # stderr=subprocess.PIPE,
            universal_newlines=True,
        )

    def close(self):
        self.process.terminate()

    def wait_for_close(self):
        self.close()
        while self.process.poll() is None:  # and (worker_process.poll() is None):
            sleep(0.1)
        sleep(0.1)
