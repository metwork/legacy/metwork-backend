# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import JsonResponse
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.decorators import action
from base.views.model_auth import IsOwnerOrPublic
from base.views.utils import MetaIdsViewSet
from fragmentation.models import FragMol
from base.modules import JSONSerializerField
from base.modules.queryset import FilteredQueryset


class FragMolSerializer(serializers.ModelSerializer):
    class Meta:
        model = FragMol
        fields = (
            "parent_mass",
            "adduct",
        )


class FragMolViewSet(MetaIdsViewSet):

    serializer_class = FragMolSerializer
    permission_classes = (IsOwnerOrPublic,)
    queryset = FragMol.objects.all()

    @action(detail=True, methods=["get"])
    def get_mgf(self, request, pk=None):
        frag_mol = self.get_object()
        mgf = frag_mol.gen_mgf()
        return JsonResponse({"mgf": mgf}, safe=False)
